enum ReportItemType {
    ReportRoot,
    ReportSection,
    TextBox,
    Table,
    Graph
}

export default ReportItemType